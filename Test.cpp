#include "Test.h"
#include <iostream>
#include "Mem.h"
#include "Tree.h"
#include <vector>
#include <numeric>
#include <ctime>

int Test::testFailed(string test_name)
{
    std::cout<<test_name + " Fail" <<std::endl;
	return 0;
}

int Test::testPassed(string test_name)
{   
    std::cout<<test_name + " Passed"<<std::endl;
    return 0;
}

int Test::testSimpleTestFrom1to12Elem()
{
    string test_name = "Simple Test 3 first child with three childs ";
    Mem mem(100);
    Tree tree(mem);
    Tree::Iterator* iterator = tree.begin();
    list<int> lt = {0, 1, 2};
	unsigned int time = clock();
    for (auto l : lt)
    {
        if (tree.insert(iterator, 0, 0, 3-l))
        {
            std::cout<<"insert "<< l << " fail"<<std::endl;
            return testFailed(test_name);
        }        
    }
	std::cout << "added three first childs our root: done" << std::endl;

	//*************************************************************************
    int counter = 4;
    for (int i = 0; i < 3; i++)
    {
        if (iterator->goToChild(i) == 0)
        {
            std::cout<<"cannot go to "<< i <<std::endl;
            return testFailed(test_name);
        }
        for (auto l : lt)
        {
	        if (tree.insert(iterator, l, NULL, counter++) == 1)
	        {
				std::cout << "insert " << l << " failed" << std::endl;
				return testFailed(test_name);
	        }
        }
        if (iterator->goToParent() == 0)
        {
			std::cout << "cannot go back from" << i << std::endl;
			return testFailed(test_name);
        }       
        
    }
	std::cout << "time to add all items = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "we add three children to each child: done" << std::endl;
	//*************************************************************************
	//��������� �� ������������� ����� ���������
	//� ������ � ������� ������� ���� 4, 5, 6
	//� ������� ������� ���� 7, 8, 9
	//� �������� ������� ���� 10, 11, 12
	
	vector<int> vt = { 1, 4, 5, 6, 2, 7, 8, 9, 3, 10, 11, 12 };
    counter = 0;

    while (iterator->hasNext())
    {
        iterator->goToNext();
        size_t size;
        iterator->getElement(size);
        if(vt[counter] != size)
        {
            std::cout<<"error in traversing with "<<counter<<std::endl; 
            return testFailed(test_name);
        }
        counter++;
    }
	std::cout << "checking for the existence of elements: done" << std::endl;
	//*************************************************************************
	//�������� ������� ������� ����� � �������� ��� ���������
	
    iterator = tree.begin();
    if (iterator->goToChild(0) == 0)
    {
        std::cout<<"cannot go to 1"<<std::endl;
        return testFailed(test_name);
    }
    for (size_t i = 0; i < 2; i++)
    {
		std::cout << "Check Leaf Only with 1 and 0" << std::endl;
        if(tree.remove(iterator, 1) == 1)
        {
            std::cout<<"deleted "<<i+1<<" with error leaf only"<<std::endl;
            return testFailed(test_name);
		}
		else
		{
			std::cout << "With leaf only = 1 : Done" << std::endl;
		}
    	
        if (tree.remove(iterator, 0) == 0)
        {
            std::cout<<"cannot delete "<<i+1<<std::endl;
            return testFailed(test_name);
        }
		else
		{
			std::cout << "With leaf only = 0 : Done" << std::endl;
		}
    }

	//�������� ������ ��������� �� 3 ������� � ���� ���� 10, 11, 12
    if (iterator->goToChild(2) == 0)
    {
        std::cout<<"cannot go to 12"<<std::endl;
        return testFailed(test_name);
    }
	else
	{
		std::cout << "moved to element 12 : Done" << std::endl;
	}
	//������� 11 �������
    if (tree.remove(iterator, 1) == 0)
    {
        std::cout<<"cannot delete 12"<<std::endl;
        return testFailed(test_name);
    }
	else
	{
		std::cout << "deleted 11 element : Done" << std::endl;
	}

	
	//*************************************************************************
	
	std::cout << "checking the remaining items 3, 10, 11" << std::endl;
    vt = {3, 10, 11};
    counter = 0;
    if (tree.size() != 3)
    {
        std::cout<<"wrong tree size"<<std::endl;
        return testFailed(test_name);
    }
    iterator = tree.begin();
    while (iterator->hasNext())
    {
        iterator->goToNext();
        size_t size;
        iterator->getElement(size);
        if(vt[counter] != size)
        {
            std::cout<<"error in traversing with "<<counter<<std::endl; 
            return testFailed(test_name);
        }
        counter++;
    }
    for(auto v : vt)
    {
        iterator = tree.find(NULL, v);
        if (iterator->getCurPointer() == nullptr)
        {
            std::cout<<"not found "<<v<<std::endl;
            return testFailed(test_name);

        }
        
    }
	std::cout << "checking remaining elements : done" << std::endl;
	
	//*************************************************************************
	std::cout << "checking for deleted items " << std::endl;
    vt = { 1, 4, 5, 6, 2, 7, 8, 9, 12};
    for (auto v: vt)
    {
		std::cout << v << " ";
        iterator = tree.find(NULL, v);
        if (iterator->getCurPointer() != NULL)
        {
            std::cout<<"found "<<v<<std::endl;
            return testFailed(test_name);
        }        
    }
	std::cout << std::endl;
	std::cout << "checking for deleted items : Done " << std::endl;

	//**************************************************************************
    return testPassed(test_name);
    
}

int Test::testOn10000FirstElements()
{
    string test_name = "Test on 10000 first elements";
    Mem mem(10000);
    Tree tree(mem);
    Tree::Iterator* iterator = tree.begin();
    vector<int> vt(10000);
    std::iota(vt.begin(), vt.end(), 0);//������� ������ �� 10000 ��������� � ��������� ��� ���� ������
	int i = 0;
	std::cout << "add 10000 first child elements in our tree" << std::endl;
	unsigned time = clock();
	for (auto &v: vt)
    {
		i++;
        if (tree.insert(iterator, v, &v, i) == 1)
        {
            std::cout<<"insert "<<v<<" fail"<<std::endl;
            return testFailed(test_name);
        }        
    }
	std::cout << "time to add 10000 items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "added 10000 elements : Done" << std::endl;
	//**********************************************************************************
	std::cout << "checking for the existence of added elements" << std::endl;
	iterator = tree.begin();
    int counter = 0;
    while(iterator->hasNext())
    {
        iterator->goToNext();
        size_t size;
        iterator->getElement(size);
        int v = *static_cast<int*>(iterator->getElement(size));
        if (vt[counter++] != v)
        {
            std::cout<<"error in comparing values with "<<counter<<std::endl;
            return testFailed(test_name);
        }        
    }
	std::cout << "Checking elements : Done" << std::endl;
	//**********************************************************************************
	std::cout << "go to first element for first Iterator" << std::endl;
    iterator = tree.begin();
    if(iterator->goToChild(1) == 0)
    {
        std::cout<<"cannot go to 1"<<std::endl;
        return testFailed(test_name);
    }
	else
	{
		std::cout << "go to first element : Done" << std::endl;
	}
	std::cout << "go to element zero for second Iterator" << std::endl;
    Tree::Iterator *niterator = tree.begin();
    if (niterator->goToChild(0) == 0)
    {
        std::cout<<"cannot go to 0"<<std::endl;
        return testFailed(test_name);
    }
	else
	{
		std::cout << "go to element zero for second Iterator : Done" << std::endl;
	}
	std::cout << "go to element first for second Iterator" << std::endl;
    if (niterator->hasNext() == 0)
    {
        std::cout<<"cannot go to 1";
        return testFailed(test_name);
    }
	else
	{
		std::cout << "go to element first for second Iterator : Done" << std::endl;
	}
    niterator->goToNext();
	std::cout << "comparing the values of two Iterators (they point to the same element)" << std::endl;
    if(iterator->equals(niterator) == 0)
    {
        std::cout<<"error with equal"<<std::endl;
        return testFailed(test_name);
    }
	else
	{
		std::cout << "they are equal Done" << std::endl;
	}
    return testPassed(test_name);
}



int Test::test1000RootChildWithEach1000Child()
{
    string test_name = "Test on 1 000 000 elements";
    Mem mem(1000000);
    Tree tree(mem);
    Tree::Iterator *iterator = tree.begin();
    const int N = 1000;
    vector<int> vt(N);
    iota(vt.begin(), vt.end(), 0);
	//****************************************************************************************
	std::cout << "add first 1000 elements from 0 to 999000 (0, 1000, 2000, ... 999000)" << std::endl;
	unsigned int time = clock();
	for (auto& v: vt)
    {
		int tempSize = N * (N - 1) - v * N;
        if(tree.insert(iterator, 0, NULL, tempSize)==1)
        {
            std::cout<<"insert "<<v<<" failed"<<std::endl;
            return testFailed(test_name);
        }		
    }
	std::cout << "time to add 1000 items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "added first 1000 elements : Done" << std::endl;
	//****************************************************************************************
	std::cout << "add 1000 children to each root child" << std::endl;
	//������� ��� �� ��������� � ����� 999 998 997 ... 1 
	vt.pop_back();
	for (int i = 0; i < N; i++)
    {
        if (iterator->goToChild(i) == 0)
        {
            std::cout<<"cannot go to "<<i<<std::endl;
            return testFailed(test_name);
        }
        for (auto v: vt)
        {
            int last = (i + 1) * N-1;
            if (tree.insert(iterator, 0, NULL, last - v) == 1)
            {
                std::cout<<"insert "<<v<<" failed"<<std::endl;
                return testFailed(test_name);
            }
        }
        if (iterator->goToParent() == 0)
        {
            std::cout<<"cannot go back from"<<i<<std::endl;
            return testFailed(test_name);
        }        
    }
	std::cout << "time to add " << N * N << " items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "Added 1,000 children for each root child : Done" << std::endl;

	//****************************************************************************************
	std::cout << "Check size our tree size" << std::endl;

	vt.resize(N*N);
    iota(vt.begin(), vt.end(), 0);
    int counter = 0;
		
    if (tree.size() != N*N)
    {
        std::cout<<"wrong tree size"<<std::endl;
        return testFailed(test_name);
    }
	std::cout << "Our tree size = " << N * N << " : Done" << std::endl;

	//****************************************************************************************
	std::cout << "passage through our tree" << std::endl;
	iterator = tree.begin();
    while (iterator->hasNext())
    {
        iterator->goToNext();
        size_t size; 
        iterator->getElement(size);
        if (vt[counter] != size)
        {
            std::cout<<"error in traversing with "<<counter<<std::endl;
            return testFailed(test_name);
        }
        counter++;        
    }
	std::cout << "passage through our tree: Done" << std::endl;
    return testPassed(test_name);    
}

int Test::testOn10000000RootChild()
{
	string test_name = "Test Remove Elements";
	Mem mem(10000000);
	Tree tree(mem);
	Tree::Iterator* iterator = tree.begin();
	int N = 1e7;
	vector<int> vt(N);
	iota(vt.begin(), vt.end(), 0);
	
	//� ������� ������� 10 ��� ���������
	//**************************************************************************

	std::cout << "add 10 mln root children elements" << std::endl;
	unsigned int time = clock();
	for (auto& v : vt)
	{
		if (tree.insert(iterator, 0, &v, 0) == 1)
		{
			std::cout << "insert " << v << " failed" << std::endl;
			return testFailed(test_name);
		}
	}
	std::cout << "time to add 10 mln items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "add 10 mln root children elements : Done" << std::endl;
	//**************************************************************************
	std::cout << "passage through our tree and remove every element from end" << std::endl;
	iterator = tree.begin();
	int index = N - 1;
	time = clock();
	while (iterator->hasNext())
	{
		iterator->goToNext();
		tree.remove(iterator);
		index--;
		size_t size;
		int v = *static_cast<int*>(iterator->getElement(size));
		if (index < 0)
		{
			std::cout << "error in deleting" << std::endl;
			return testFailed(test_name);
		}
		if (vt[index] != v)
		{
			std::cout << "error in comparing values with" << vt[index] << " " << v << std::endl;
			return testFailed(test_name);
		}
		index--;
	}
	
	if (index != -1)
	{
		std::cout << "didn't go through elements" << std::endl;
		return testFailed(test_name);
	}
 	std::cout << "time to passage 10 mln items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "passage through our tree and remove every element from end : Done" << endl;
	return testPassed(test_name);

}

int Test::testGap()
{
	string test_name = "Gap from sm to sm";
	Mem mem(10000);
	Tree tree(mem);
	Tree::Iterator* iterator = tree.begin();
	vector<int> vt(10000);
	std::iota(vt.begin(), vt.end(), 0);//������� ������ �� 10000 ��������� � ��������� ��� ���� ������
	int i = 0;
	std::cout << "add 10000 first child elements in our tree" << std::endl;
	unsigned int time = clock();
	for (auto& v : vt)
	{
		i++;
		if (tree.insert(iterator, v, &v, i) == 1)
		{
			std::cout << "insert " << v << " fail" << std::endl;
			return testFailed(test_name);
		}
	}
	std::cout << "time to add 10000 items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "added 10000 elements : Done" << std::endl;

	
	iterator = tree.begin();
	const int gap_start = 57;
	const int gap_end = 6458;
	cout <<"size before removal = " <<tree.getSize_() << endl;
	time = clock();
	for ( i = gap_start; i < gap_end; i++)
	{
		iterator->goToChild(i);
		tree.remove(iterator);
	}

	std::cout << "time to remove items from point "<< gap_start <<" to "<< gap_end << " = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout<< "size after removal = " << tree.getSize_() << std::endl;
	std::cout << "passage through our tree" << std::endl;
	std::cout << "Test Gap : done" << std::endl;
	
	return testPassed(test_name);
}

int Test::testRemoveOdd1MlnItems()
{
	
	
	string test_name = "Remove odd elements";
	Mem mem(1000000);
	Tree tree(mem);
	Tree::Iterator* iterator = tree.begin();
	const int N = 1000;
	vector<int> vt(N);
	iota(vt.begin(), vt.end(), 0);
	//****************************************************************************************
	std::cout << "add first 1000 elements from 0 to 999000 (0, 1000, 2000, ... 999000)" << std::endl;
	unsigned int time = clock();
	for (auto& v : vt)
	{
		v = N * (N - 1) - v * N;
		
		if (tree.insert(iterator, 0, &v, sizeof(v)) == 1)
		{
			std::cout << "insert " << v << " failed" << std::endl;
			return testFailed(test_name);
		}		
	}
	std::cout << "time to add 1000 items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "added first 1000 elements : Done" << std::endl;
	//****************************************************************************************
	std::cout << "add 1000 children to each root child" << std::endl;
	//������� ��� �� ��������� � ����� 999 998 997 ... 1 
	vt.pop_back();
	for (int i = 0; i < N; i++)
	{
		if (iterator->goToChild(i) == 0)
		{
			std::cout << "cannot go to " << i << std::endl;
			return testFailed(test_name);
		}
		for (auto v : vt)
		{
			int last = (i + 1) * N - 1;
			v = last - v;
			
			if (tree.insert(iterator, 0, &v, sizeof(v)) == 1)
			{
				std::cout << "insert " << v << " failed" << std::endl;
				return testFailed(test_name);
			}
		}
		if (iterator->goToParent() == 0)
		{
			std::cout << "cannot go back from" << i << std::endl;
			return testFailed(test_name);
		}
	}
	std::cout << "time to add " << N * N << " items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "Added 1,000 children for each root child : Done" << std::endl;

	//***********************************************************
	time = clock();
	cout << "remove odd items" << endl;
	/*int i = 1;
	for (auto& v : vt)
	{		
		v = i;
		i++;
	}*/
	
	int i = 0;
	iterator = tree.begin();
	while (iterator->hasNext())
	{
		iterator->goToNext();
		size_t size;
		void *eq = iterator->getElement(size);
		void* lol = &vt[i];
		if (lol == eq)
		{			
			tree.remove(iterator);
			i += 2;
		}
	}
	
	std::cout << "time to remove odd items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "size after removal = " << tree.getSize_() << std::endl;

	//***********************************************************
	
	return testPassed(test_name);
}

/*int Test::testGapForChild()
{
	string test_name = "Test on 1 000 000 elements";
	Mem mem(1000000);
	Tree tree(mem);
	Tree::Iterator* iterator = tree.begin();
	const int N = 1000;
	vector<int> vt(N);
	iota(vt.begin(), vt.end(), 0);
	//****************************************************************************************
	std::cout << "add first 1000 elements from 0 to 999000 (0, 1000, 2000, ... 999000)" << std::endl;
	unsigned int time = clock();
	for (auto& v : vt)
	{
		int tempSize = N * (N - 1) - v * N;
		if (tree.insert(iterator, 0, NULL, tempSize) == 1)
		{
			std::cout << "insert " << v << " failed" << std::endl;
			return testFailed(test_name);
		}
	}
	std::cout << "time to add 1000 items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "added first 1000 elements : Done" << std::endl;
	//****************************************************************************************
	std::cout << "add 1000 children to each root child" << std::endl;
	//������� ��� �� ��������� � ����� 999 998 997 ... 1 
	vt.pop_back();
	for (int i = 0; i < N; i++)
	{
		if (iterator->goToChild(i) == 0)
		{
			std::cout << "cannot go to " << i << std::endl;
			return testFailed(test_name);
		}
		for (auto v : vt)
		{
			int last = (i + 1) * N - 1;
			if (tree.insert(iterator, 0, NULL, last - v) == 1)
			{
				std::cout << "insert " << v << " failed" << std::endl;
				return testFailed(test_name);
			}
		}
		if (iterator->goToParent() == 0)
		{
			std::cout << "cannot go back from" << i << std::endl;
			return testFailed(test_name);
		}
	}
	std::cout << "time to add " << N * N << " items  = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "Added 1,000 children for each root child : Done" << std::endl;

	//*********************************************************************
	cout << "size before removal = " << tree.getSize_() << endl;
	time = clock();
	const int gap_start = 154;
	const int gap_end = 754;
	iterator = tree.begin();
	iterator->goToChild(gap_start);
	for (int i = gap_start; i < gap_end; i++)
	{
		iterator->goToNext();
		tree.remove(iterator);
	}
	std::cout << "time to remove items from point " << gap_start << " to " << gap_end << " = " << (double)(clock() - time) / CLOCKS_PER_SEC << std::endl;
	std::cout << "size after removal = " << tree.getSize_() << std::endl;
	std::cout << "passage through our tree" << std::endl;
	std::cout << "Test Gap : done" << std::endl;
}
*/


int Test::testCheckExeptionIteratorBlock()
{
	string test_name = "Test check empty Iterator";
	Mem mem(100);
	Tree tree(mem);
	try
	{
		std::cout << "check our Iterator throught Error block" << std::endl;
		Tree::Iterator* iterator = tree.newIterator();
		size_t size;
		iterator->getElement(size);
	}
	catch (Container::Error e)
	{
		std::cout << "Our Iterator hit the exception block" << std::endl;
		string s_error = "Can't give an item because the iterator points to an empty element";
		if (s_error != e.msg)
		{
			std::cout << "other error" << std::endl;
			return testFailed(test_name);
		}
	}
	return testPassed(test_name);
}