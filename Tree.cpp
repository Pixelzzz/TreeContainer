﻿#include "Tree.h"


Tree::GTreeNode::GTreeNode(GTreeNode* parent, DATA_TYPE data, size_t size)
{
	this->parent_ptr = parent;
	this->size = size;
	this->data = data;
	this->right_b = nullptr;
	this->left_b = nullptr;
	this->first = nullptr;	
}

Tree::GTreeNode::~GTreeNode(){

}

void Tree::deleteNodeRecursive(GTreeNode* node, bool version)
{
	GTreeNode* firstChildNode = node->getFirstChild();
	while (firstChildNode != nullptr)
	{
		GTreeNode* rightBrotherNode = firstChildNode->getRight();
		deleteNodeRecursive(firstChildNode, 1);
		firstChildNode = rightBrotherNode;
	}
	node->setFirstChild(nullptr);
	if (version)
	{
		sizeChange(-1);
		free_mem(node);
	}	
}

//Realese Iterator class**************************************

Tree::GTreeNode* Tree::Iterator::step(GTreeNode* cur)
{
	GTreeNode* child = cur;
	if (child->getFirstChild() != nullptr)
	{
		return child->getFirstChild();
	}
	while (child != nullptr)
	{
		if (child->getRight() != nullptr)
		{
			return child->getRight();
		}
		child = child->getParent();		
	}	
	return nullptr;
}

Tree::Iterator::Iterator()
{
	this->curPointer = nullptr;	
}

Tree::Iterator::Iterator(GTreeNode* curPoint)
{
	this->curPointer = curPoint;	
}

Tree::Iterator::~Iterator(){}

void* Tree::Iterator::getElement(size_t &size)
{
	if(curPointer == nullptr)
	{
		throw Container::Error("Can't give an item because the iterator points to an empty element");
	}
	size = curPointer->getSize();
	return curPointer->getData();
}

bool Tree::Iterator::hasNext()
{
	if (curPointer == nullptr)
	{
		return false;
	}
	if (step(curPointer) == nullptr)
	{
		return false;
	}	
	return true;
}

void Tree::Iterator::goToNext()
{
	if (!hasNext())
	{
		throw Container::Error("no next item");
	}
	curPointer = step(curPointer);	
}

bool Tree::Iterator::equals(Container::Iterator *right)
{
	if((dynamic_cast<Tree::Iterator*>(right))->curPointer == this->curPointer)
	{
		return true;
	}
	return false;
}

bool Tree::Iterator::goToParent()
{
	if((curPointer == nullptr) || (curPointer->getParent() == nullptr))
	{
		return false;
	}
	curPointer = curPointer->getParent();
	return true;
}


bool Tree::Iterator::goToChild(int child_index)
{
	GTreeNode* childs = curPointer->getFirstChild();
	for (int i = 0; i < child_index; i++)
	{
		if(childs == nullptr)
		{
			return false;
		}
		childs = childs->getRight();
	}
	if (childs == nullptr)
	{
		return false;
	}
	curPointer = childs;		
	return false;	
}

//***********************************************************************

Tree::Tree(MemoryManager &mem) : AbstractTree(mem)
{	
	this->_memory = mem;
	root_ = new(alloc_mem(sizeof(GTreeNode))) GTreeNode(nullptr, nullptr, 0);	
	size_ = 0;
}

Tree::~Tree(){
	clear();	
}


void* Tree::alloc_mem(size_t size)
{
	return _memory.allocMem(size);
}

void Tree::free_mem(void* ptr)
{
	_memory.freeMem(ptr);
}



int Tree::insert(AbstractTree::Iterator* iter, int child_index, void* elem, size_t size)
{
	Tree::Iterator* iterator = dynamic_cast<Tree::Iterator*>(iter);
	if ((iterator->getCurPointer() == nullptr) || (child_index < 0))
	{
		return 1;	
	}	
	GTreeNode* node = iterator->getCurPointer();
	GTreeNode* newChild = new (alloc_mem(sizeof(GTreeNode))) GTreeNode(node, elem, size);
	GTreeNode* firstChild = node->getFirstChild();
	if (child_index == 0)
	{
		node->setFirstChild(newChild);
		newChild->setRight(firstChild);
		if (firstChild != nullptr)
		{
			firstChild->setLeft(newChild);
		}		
	}
	else
	{
		for (int i = 0; i < child_index - 1; i++)
		{
			firstChild = firstChild->getRight();
			if(firstChild == nullptr)
			{
				return 1;
			}
		}
		GTreeNode* rightBrother = firstChild->getRight();
		firstChild->setRight(newChild);
		newChild->setLeft(firstChild);
		newChild->setRight(rightBrother);
		if(rightBrother != nullptr)
		{
			rightBrother->setLeft(newChild);
		}		
	}	
	sizeChange(1);
	return 0;	
}


bool Tree::remove(AbstractTree::Iterator* iter, int leaf_only)
{
	Tree::Iterator *iterator = dynamic_cast<Tree::Iterator*>(iter);
	GTreeNode* node = iterator->getCurPointer();
	GTreeNode* tmp;
	if (leaf_only == 1 && node->getFirstChild() != nullptr)
	{
		return false;
	}

	if (node->getRight() != nullptr)
	{
		tmp = node->getLeft();
		node->getRight()->setLeft(tmp);
	}	

	if (node->getLeft() != nullptr)
	{
		tmp = node->getRight();
		node->getLeft()->setRight(tmp);
	}

	if ((node->getParent() != nullptr) && (node->getParent()->getFirstChild() == node))
	{
		tmp = node->getParent();
		tmp->setFirstChild(node->getRight());
	}	

	deleteNodeRecursive(node, 0);
	if (iterator->hasNext())
	{
		iterator->goToNext();
	}
	else
	{
		iterator->setCurPointer(nullptr);
	}
	deleteNodeRecursive(node, 1);	
	return true;
}

int Tree::size(){
	return size_;
}

size_t Tree::max_bytes()
{
	return _memory.size();
}

Tree::Iterator *Tree::find(void* elem, size_t size)
{
	Tree::Iterator *iterator = new (alloc_mem(sizeof(Iterator))) Iterator(root_);
	while (iterator->hasNext())
	{
		iterator->goToNext();
		size_t tempSize;
		DATA_TYPE tempElem = iterator->getElement(tempSize);
		if ((tempElem == elem) && tempSize == size)
		{
			return iterator;
		}		
	}
	return newIterator();
}

auto Tree::newIterator() -> Tree::Iterator*
{
	return new (alloc_mem(sizeof(Iterator))) Iterator();
}

Tree::Iterator *Tree::begin()
{
	return new (alloc_mem(sizeof(Iterator))) Iterator(root_);	
}

Tree::Iterator *Tree::end()
{
	return newIterator();
}

void Tree::remove(Container::Iterator* iter)
{
	if(this->empty())
	{
		return ;
	}
	Tree::Iterator *iterat = dynamic_cast<Tree::Iterator*>(iter);
	remove(iterat, 1);
}

void Tree::clear()
{
	if(this->empty())
	{
		return;
	}

	Tree::Iterator* it = new Iterator(root_);
	remove(it, 0);
	root_ = new (alloc_mem(sizeof(GTreeNode))) GTreeNode(nullptr, nullptr, 0);
	size_ = 0;
}

bool Tree::empty()
{
	if(size_ == 0)
	{
		return true;
	}
	return false;
}