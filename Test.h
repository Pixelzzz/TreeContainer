#include "Tree.h"

class Test
{
private:
    int testFailed(string test_name);
    int testPassed(string test_name);
public:

	int testSimpleTestFrom1to12Elem();
	int testOn10000FirstElements();
	int testCheckExeptionIteratorBlock();
    int test1000RootChildWithEach1000Child();
	int testOn10000000RootChild();
	int testGap();
	int testGapForChild();
	int testRemoveOdd1MlnItems();
	
    
};