﻿#pragma once
#include "TreeAbstract.h"
#include <list>
#include <iostream>
#include <vector>

class Tree: public AbstractTree                       
{
private:
	typedef void* DATA_TYPE;
	
	class GTreeNode{
	private:
		size_t size;
		
		DATA_TYPE data;
		GTreeNode* parent_ptr;
		GTreeNode* first_ptr;
		GTreeNode* right_ptr;
		GTreeNode* left_ptr;
		
	public:		
		GTreeNode(GTreeNode* parent, DATA_TYPE data, size_t size);
		~GTreeNode();		
		
		//getters and setters
		size_t getSize(){
			return size;
		}
		DATA_TYPE getData(){
			return data;
		}		
		GTreeNode* getParent(){
			return parent_ptr;
		}		

		GTreeNode* getFirst()
		{
			return first_ptr;
		}
		GTreeNode* getLeft() const
		{
			return this->left_ptr;
		}
		GTreeNode* getRight() const
		{
			return this->right_ptr;
		}
		void setFirst(GTreeNode* first)
		{
			this->first_ptr = first;
		}
		void setLeft(GTreeNode* left)
		{
			this->left_ptr = left;
		}
		void setRight(GTreeNode* right)
		{
			this->right_ptr = right;
		}		
	};
	
	GTreeNode *root_; 
	size_t size_;	
	//private methods
	void deleteNodeRecursive(GTreeNode* node, bool version);
	void sizeChange(int someSize)
	{
		size_ += someSize;
	}

public:	
	
	class Iterator: public AbstractTree::Iterator
	{
	private:
		GTreeNode* curPointer; 
		
		GTreeNode* step(GTreeNode* cur);
		

	public:
		//constructors and destuctor
		Iterator();
		Iterator(GTreeNode* curPoint);
		~Iterator();

		//gettets and setters
		GTreeNode* getCurPointer()
		{
			return curPointer;
		}
		void setCurPointer(GTreeNode* point)
		{
			curPointer = point;
		}		

		//Iterator methods

		bool goToChild(int child_index) override;
		bool goToParent() override;
		DATA_TYPE getElement(size_t& size) override;
		bool hasNext() override;
		void goToNext() override;
		bool equals(Container::Iterator* right) override;
	
	};

	//constructors and destructor
	Tree(MemoryManager& mem);
	~Tree();

	//Getters and setters
	void setSize_(size_t size_) {
		this->size_ = size_;
	}

	void setRoot_(GTreeNode* root_) {
		this->root_ = root_;
	}

	size_t getSize_() {
		return size_;
	}	
	
	GTreeNode* getRoot_() {
		return root_;
	}

	//methods TreeAbstract
	int insert(AbstractTree::Iterator* iter, int child_index, void* elem, size_t size) override;
	bool remove(AbstractTree::Iterator* iter, int leaf_only) override;
	
	//methods Container

	void* alloc_mem(size_t size);
	void free_mem(void* ptr);
	
	int size() override;
	size_t max_bytes() override;
	Tree::Iterator* find(void* elem, size_t size) override;
	Tree::Iterator* newIterator() override;
	Tree::Iterator* begin() override;
	Tree::Iterator* end() override;
	void remove(Container::Iterator* iter) override;
	void clear() override;
	bool empty() override;
	
};

